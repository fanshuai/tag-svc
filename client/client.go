package main

import (
	"context"
	pb "go-programming-tour-book/tag-svc/protoc"
	"google.golang.org/grpc"
	"log"
)

func main()  {
	ctx := context.Background()
	clientConn, _ := GetClientConn(
		ctx, "localhost:8001",
		[]grpc.DialOption{grpc.WithBlock()},
	)
	defer func() {
		_ = clientConn.Close()
	}()
	tagServiceClient := pb.NewTagServiceClient(clientConn)
	resp, _ := tagServiceClient.GetTagList(ctx, &pb.GetTagListRequest{Name: ""})
	log.Printf("resp: %v", resp)
}

func GetClientConn(ctx context.Context, target string, opts []grpc.DialOption) (*grpc.ClientConn, error) {
	opts = append(opts, grpc.WithInsecure())
	return grpc.DialContext(ctx, target, opts...)
}
