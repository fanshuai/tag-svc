module go-programming-tour-book/tag-svc

go 1.16

require (
	github.com/fullstorydev/grpcurl v1.8.0 // indirect
	github.com/golang/protobuf v1.4.3
	github.com/grpc-ecosystem/grpc-gateway v1.16.0
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.3.0 // indirect
	github.com/jhump/protoreflect v1.8.2 // indirect
	github.com/opentracing/opentracing-go v1.2.0
	github.com/soheilhy/cmux v0.1.4 // indirect
	golang.org/x/net v0.0.0-20210119194325-5f4716e94777
	golang.org/x/text v0.3.5 // indirect
	google.golang.org/genproto v0.0.0-20210315142602-88120395e650 // indirect
	google.golang.org/grpc v1.36.0
	google.golang.org/protobuf v1.25.1-0.20201208041424-160c7477e0e8
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
