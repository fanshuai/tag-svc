### Protoc
```shell
protoc --go_out=plugins=grpc:. ./proto/*.proto

protoc -I/usr/local/include -I. \
-I$GOPATH/src \
-I$GOPATH/pkg/mod/github.com/grpc-ecosystem/grpc-gateway@v1.16.0/third_party/googleapis \
--grpc-gateway_out=logtostderr=true:. \
./proto/*.proto
```


### gRPCurl
```shell
grpcurl -plaintext localhost:8001 list

grpcurl -plaintext localhost:8001 list TagService

grpcurl -plaintext -d '{"name":""}' localhost:8001 TagService.GetTagList
```

### grpc-gateway
```shell
go get -u github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway
go install github.com/grpc-ecosystem/grpc-gateway/protoc-gen-grpc-gateway



```